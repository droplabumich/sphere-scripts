
container_name="ROS_DEVEL"

CONTAINER_RUNNING=$(docker inspect -f '{{.State.Running}}' $container_name)
echo $CONTAINER_RUNNING
echo $CONTAINER_EXITED

docker rm $(docker ps --filter "status=exited" -q)



if [ $CONTAINER_RUNNING == 'true' ]
then
	docker exec -it $container_name /bin/bash
else
	docker run --net host --name $container_name -it \
		-v $PWD/../../:/mbed_ws/data/ \
		-v /dev:/dev \
		--privileged  \
		-e DISPLAY=$DISPLAY \
        --volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
		sphere:test bash
fi