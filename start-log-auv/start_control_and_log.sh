#!/bin/bash
trap "exit" INT TERM ERR
trap "kill 0" EXIT
echo "Running controller .."
sh start_control.sh &
sleep 2
echo "Save params "
sh param_server_log.sh
wait
