#!/bin/bash
## Save all ROS parameters to a given folder. If no folder is given, the default is used
if [ "$1" != "" ]; then
    path="$1"
else
    echo "Using default path for logs"
    path="$HOME/ros_logs/misc/"
fi

_now=$(date -u +"%Y_%m_%d_%H%M%S")
_file="params_$_now.yaml"
echo "Saving parameters to $path/$_file..."
mkdir -p "$path/$file"
rosparam dump "$path/$_file"
