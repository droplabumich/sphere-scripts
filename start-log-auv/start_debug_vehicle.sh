#!/bin/bash
trap "exit" INT TERM ERR
trap "kill 0" EXIT
echo "Running vehicle .."

_now=$(date -u +"%Y_%m_%d_%H%M%S")
logpath="$HOME/ros_logs/$_now"
mkdir -p "$logpath"
sh start_vehicle.sh "$logpath" &
sleep 10
echo "Running controller .."
sh start_control_debug.sh &
sleep 10
echo "Save params "
sh param_server_log.sh "$logpath"
wait
