#!/bin/bash
trap "exit" INT TERM ERR
trap "kill 0" EXIT
echo "Running vehicle .."
sh start_vehicle.sh &
sleep 2
echo "Running controller .."
sh start_control.sh &
sleep 2
echo "Running path & loading waypoints .."
sh start_path.sh & 
sleep 5
sh load_path.sh
sleep 2
echo "Save params "
sh param_server_log.sh
wait
