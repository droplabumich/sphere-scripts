#!/bin/bash  
rosrun camera_calibration cameracalibrator.py  --size 8x6 --square 0.04 left:=/stereo_in/left/image_color right:=/stereo_in/right/image_raw left_camera:=/stereo_in/left right_camera:=/stereo_in/right --approximate=1
