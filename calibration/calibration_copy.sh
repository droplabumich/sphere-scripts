#!/bin/bash  
echo "This is a script for copying calibration files onto sphere"
echo "Unzip the calibration files"
tar -xvf /tmp/calibrationdata.tar.gz left.yaml right.yaml
echo "Rename files with their serial numbers: left - 15662055; right - 15435630"
mv left.yaml 15662055.yaml
mv right.yaml 15435630.yaml
echo "Rename camera names inside the calibration files"
rpl -i -w "narrow_stereo/left" "15662055" 15662055.yaml
rpl -i -w "narrow_stereo/right" "15435630" 15435630.yaml
echo "Copying calibration files over to odroid - pointgrey_camera_driver/cfg "
scp 15662055.yaml odroid@141.212.194.156:/home/odroid/odroid-development/catkin_ws/src/pointgrey_camera_driver/pointgrey_camera_driver/cfg
scp 15435630.yaml odroid@141.212.194.156:/home/odroid/odroid-development/catkin_ws/src/pointgrey_camera_driver/pointgrey_camera_driver/cfg

