#!/bin/bash
if [ "$(whoami)" == "root" ]; then
        echo "Script must be run as user, not root"
        exit -1
fi
sudo adduser $(whoami) dialout