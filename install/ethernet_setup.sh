#!/bin/bash

# This scipt creates a wired network called SphereInternetSharing that 
# enables the sharing of a laptops wifi internet connection with the 
# vehicle through lan

ethDevice=''

get_iface () {
local ethString='ethernet'
local cmd="nmcli --terse --fields DEVICE,TYPE dev status" 
#echo "Setting up new network configuration"
$cmd | while read -r device_type;
do
	IFS=: read device ifacetype <<< "$device_type"
	#echo "$ifacetype"
	if [ "$ifacetype" == "$ethString" ];
    then
 #       echo "Found ethernet adapter $device"
        echo $device
        break
    fi
done
}

device_name=$(get_iface)
echo $device_name
echo $ethDevice
#nmcli con add con-name SphereTest ifname $device_name type ethernet ip4 141.212.194.2/24
nmcli con add con-name SphereInternetSharing ifname $device_name type ethernet ip4 141.212.194.129/24 
nmcli connection modify SphereInternetSharing ipv4.method "Shared"


